-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 13, 2018 at 09:59 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `appointment`
--

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `Username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Fname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gender` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `SID` int(5) NOT NULL,
  `MID` int(5) NOT NULL,
  `DOV` date NOT NULL,
  `Timestamp` datetime NOT NULL,
  `Status` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`Username`, `Fname`, `Gender`, `SID`, `MID`, `DOV`, `Timestamp`, `Status`) VALUES
('Aldi5we', 'ÐœÐ°Ð»Ð¸Ðº ÐÐ»Ð´Ð¸ÑÑ€', 'female', 1, 1, '2018-11-14', '2018-11-13 04:09:23', 'Cancelled by Aldi5we'),
('user', 'Ernar', 'male', 1, 1, '2018-11-19', '2018-11-12 22:20:43', 'Cancelled by user'),
('Aldi5we', 'Aldi', 'male', 1, 1, '2018-11-14', '2018-11-13 04:07:41', 'Cancelled by Aldi5we'),
('user', 'Ernar', 'male', 1, 1, '2018-11-14', '2018-11-12 22:19:48', 'Cancelled by user'),
('user', 'ÐœÐ°Ð»Ð¸Ðº ÐÐ»Ð´Ð¸ÑÑ€', 'female', 123, 13, '2018-11-14', '2018-11-13 09:17:03', 'Cancelled by user');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `contact` bigint(20) NOT NULL,
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`name`, `gender`, `dob`, `contact`, `email`, `username`, `password`) VALUES
('user', 'male', '1985-01-01', 7897897897, 'user@test.com', 'user', 'user'),
('ÐœÐ°Ð»Ð¸Ðº ÐÐ»Ð´Ð¸ÑÑ€', 'female', '2018-01-13', 122132123132, 'al@a.s', 'Aldi5we', '123');

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `mid` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `contact` bigint(20) NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `region` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`mid`, `name`, `gender`, `dob`, `contact`, `address`, `username`, `password`, `region`) VALUES
(1, 'Manager', 'male', '1990-01-01', 8888899999, 'XYZ complex CST', 'manager', 'manager', 'U'),
(9, 'managger', 'female', '2018-11-09', 7774412354, 'Addresi', 'managger', '123', 'U'),
(199, 'ÐÐ»Ð´Ðµ Ð±ÑÑ‚Ñ‡', 'female', '2018-11-12', 1567459519, 'ÐÐ»Ð´Ðµ Ð±ÑÑ‚Ñ‡ Ð¿Ñ€Ð¾ÑÐ¿ÐµÐºÑ‚', 'ÐÐ»Ð´ÐµÐ±ÑÑ‚Ñ‡', '123', 'U');

-- --------------------------------------------------------

--
-- Table structure for table `manager_salon`
--

CREATE TABLE `manager_salon` (
  `sid` int(11) DEFAULT NULL,
  `mid` int(11) NOT NULL,
  `town` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manager_salon`
--

INSERT INTO `manager_salon` (`sid`, `mid`, `town`) VALUES
(1, 0, NULL),
(1, 9, NULL),
(1, 199, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master`
--

CREATE TABLE `master` (
  `mid` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `experience` int(11) NOT NULL,
  `specialization` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `contact` bigint(20) NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `region` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `master`
--

INSERT INTO `master` (`mid`, `name`, `gender`, `dob`, `experience`, `specialization`, `contact`, `address`, `username`, `password`, `region`) VALUES
(1156, 'Aldiar', 'female', '2018-11-13', 156, 'Manikiur', 7717154659, 'Manikurova 156', 'Aldiar', '123', 'U'),
(13, 'Jacky', 'female', '2018-01-13', 11, 'Manikur', 7714562452, 'KABANBAI BATYRA', 'Jacky', '123', 'U'),
(969, 'Batyr', 'female', '2018-12-13', 56, 'Manikur', 7747095582, 'Kabanbai Batyr', 'Batyr', '123', 'Ð¨Ñ‹Ð¼ÐºÐµÐ½Ñ‚');

-- --------------------------------------------------------

--
-- Table structure for table `master_availability`
--

CREATE TABLE `master_availability` (
  `sid` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `day` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `starttime` time NOT NULL,
  `endtime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_availability`
--

INSERT INTO `master_availability` (`sid`, `mid`, `day`, `starttime`, `endtime`) VALUES
(1, 1156, 'Saturday', '00:00:00', '12:59:00'),
(123, 13, 'Friday', '07:57:00', '19:57:00'),
(123, 13, 'Monday', '07:57:00', '19:57:00'),
(123, 13, 'Thursday', '07:57:00', '19:57:00'),
(123, 13, 'Tuesday', '07:57:00', '19:57:00'),
(123, 13, 'Wednesday', '07:57:00', '19:57:00');

-- --------------------------------------------------------

--
-- Table structure for table `salon`
--

CREATE TABLE `salon` (
  `sid` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `town` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` bigint(20) NOT NULL,
  `mid` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salon`
--

INSERT INTO `salon` (`sid`, `name`, `address`, `town`, `city`, `contact`, `mid`) VALUES
(123, 'SALON', 'SALONADRESS', 'RAION', 'ÐÐ»Ð¼Ð°Ñ‚Ñ‹', 7714540265, ''),
(1, 'Ð”Ð¶Ð°ÐºÑƒÐ·Ð¸', 'Ð”Ð¶Ð°ÐºÑƒÐ²Ð·ÑÐºÐ¸Ð¹ 42', 'Ð‘Ð¾Ð³ÐµÐ½Ð±Ð°Ð¹ Ð±Ð°Ñ‚Ñ‹Ñ€Ð°', 'Ð¨Ñ‹Ð¼ÐºÐµÐ½Ñ‚', 7777777777, '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`Username`,`Fname`,`DOV`,`Timestamp`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`email`,`username`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `manager_salon`
--
ALTER TABLE `manager_salon`
  ADD PRIMARY KEY (`mid`) USING BTREE;

--
-- Indexes for table `master`
--
ALTER TABLE `master`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `master_availability`
--
ALTER TABLE `master_availability`
  ADD PRIMARY KEY (`sid`,`mid`,`day`,`starttime`,`endtime`);

--
-- Indexes for table `salon`
--
ALTER TABLE `salon`
  ADD PRIMARY KEY (`sid`,`name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
