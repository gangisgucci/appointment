<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="adminmain.css"> 
<style>
table{
    width: 75%;
    border-collapse: collapse;
	border: 4px solid black;
    padding: 5px;
	font-size: 25px;
}

th{
border: 4px solid black;
	background-color: #4CAF50;
    color: white;
	text-align: left;
}
tr,td{
	border: 4px solid black;
	background-color: white;
    color: black;
}
</style>

</head>
<body>
<div id= "main5">
<ul>
<li class="dropdown"><font color="yellow" size="10">Режим Админа</font></li>
<br>
<h2>
  <li class="dropdown">    
  <a href="javascript:void(0)" class="dropbtn">Мастер</a>
    <div class="dropdown-content">
      <a href="addmaster.php">Добавить Мастера</a>
      <a href="deletemaster.php">Удалить Мастера</a>
      <a href="showmaster.php">Список Мастеров</a>
	  <a href="showmasterschedule.php">Календарь Мастеров</a>
    </div>
  </li>
  
  <li class="dropdown">
  <a href="javascript:void(0)" class="dropbtn">Салон</a>
    <div class="dropdown-content">
      <a href="addsalon.php">Добавить Салон</a>
      <a href="deletesalon.php">Удалить Салон</a>
      <a href="addmastersalon.php">Добавить Мастера в Салон</a>
	   
	  <a href="deletemastersalon.php">Удалить Мастера с Салона</a>
	   
	  <a href="showsalon.php">Список Салонов</a>
    </div>
  </li>
 <li class="dropdown">    
		  <a href="javascript:void(0)" class="dropbtn">Менеджер</a>
			<div class="dropdown-content">
			  <a href="addmanagersalon.php">Добавить Менеджера в Салон</a>
			  <a href="deletemanagersalon.php">Удалить Менеджера из Салона</a>
			  <a href="addmanager.php">Добавить Менеджера</a>
			  <a href="deletemanager.php">Удалить Менеджера</a>
			  <a href="showmanager.php">Список Менеджеров</a>
    </div>
  </li>
   <li>  
	<form method="post" action="mainpage.php">	
	<button type="submit" class="cancelbtn" name="logout" style="float:right;font-size:22px"><b>Выйти</b></button>
	</form>
  </li>
	
</ul>
</h2>
<center><h1>Список Мастеров</h1>
<?php
session_start();
$con = mysqli_connect('localhost','root','','appointment');
if (!$con)
{
    die('Could not connect: ' . mysqli_error($con));
}
$sql="SELECT * FROM Master order by MID ASC";
$result = mysqli_query($con,$sql);
echo "<br><h2>Количество Мастеров в Базе: <b>".mysqli_num_rows($result)."</b></h2><br>";
echo "<table>
<tr>
<th>Мастер ID</th>
<th>Имя Мастера</th>
<th>Дата бронирования</th>
<th>Опыт работы</th>
<th>Специальность</th>
<th>Адрес</th>
<th>Контактные данные</th>
<th>Район</th>
</tr>";
while($row = mysqli_fetch_array($result)) {
    echo "<tr>";
	echo "<td>" . $row['mid'] . "</td>";
    echo "<td>" . $row['name'] . "</td>";
    echo "<td>" . $row['dob'] . "</td>";
    echo "<td>" . $row['experience'] . "</td>";
    echo "<td>" . $row['specialization'] . "</td>";
	echo "<td>" . $row['address'] . "</td>";
	echo "<td>" . $row['contact'] . "</td>";
	echo "<td>" . $row['region'] . "</td>";
    echo "</tr>";
}
echo "</table>";
mysqli_close($con);
if(isset($_POST['logout'])){
		session_unset();
		session_destroy();
		header( "Refresh:1; url=alogin.php"); 
	}
?>
</div>
</body>
</html>