<html>
<head>
<script src="jquerypart.js" type="text/javascript"></script>
<link rel="stylesheet" href="adminmain.css"> 
<script>
function getState(val) {
	$.ajax({
	type: "POST",
	url: "getsalon.php",
	data:'city='+val,
	success: function(data){
		$("#salon-list").html(data);
	}
	});
}
function getMasterRegion(val) {
	$.ajax({
	type: "POST",
	url: "getmasterregion.php",
	data:'city='+val,
	success: function(data){
		$("#master-list").html(data);
	}
	});
}

</script>
</head>
<body>
<div id ="main8">
<ul>
<li class="dropdown"><font color="yellow" size="10">Режим Админа</font></li>
<br>
<h2>
  <li class="dropdown">    
  <a href="javascript:void(0)" class="dropbtn">Мастер</a>
    <div class="dropdown-content">
      <a href="addmaster.php">Добавить Мастера</a>
      <a href="deletemaster.php">Удалить Мастера</a>
      <a href="showmaster.php">Список Мастеров</a>
	  <a href="showmasterschedule.php">Календарь Мастеров</a>
    </div>
  </li>
  
  <li class="dropdown">
  <a href="javascript:void(0)" class="dropbtn">Салон</a>
    <div class="dropdown-content">
      <a href="addsalon.php">Добавить Салон</a>
      <a href="deletesalon.php">Удалить Салон</a>
      <a href="addmastersalon.php">Добавить Мастера в Салон</a>
	   
	  <a href="deletemastersalon.php">Удалить Мастера с Салона</a>
	   
	  <a href="showsalon.php">Список Салонов</a>
    </div>
  </li>
 <li class="dropdown">    
	  <a href="javascript:void(0)" class="dropbtn">Менеджер</a>
		<div class="dropdown-content">
		  <a href="addmanagersalon.php">Добавить Менеджера в Салон</a>
		  <a href="deletemanagersalon.php">Удалить Менеджера из Салона</a>
		  <a href="addmanager.php">Добавить Менеджера</a>
		  <a href="deletemanager.php">Удалить Менеджера</a>
		  <a href="showmanager.php">Список Менеджеров</a>
		</div>
	  </li>
   <li>  
	<form method="post" action="mainpage.php">	
	<button type="submit" class="cancelbtn" name="logout" style="float:right;font-size:22px"><b>Выйти</b></button>
	</form>
  </li>
	
</ul>
</h2>
<center><h1>Добавить Мастера в Салон</h1><hr>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
<label style="font-size:20px" >Город:</label>
		<select name="city" id="city-list" class="demoInputBox"  onChange="getState(this.value);getMasterRegion(this.value);">
		<option value="">Выберите Город</option>
		<?php
		include 'dbconfig.php';
		$sql1="SELECT distinct City FROM salon";
         $results=$conn->query($sql1); 
		while($rs=$results->fetch_assoc()) { 
		?>
		<option value="<?php echo $rs["City"]; ?>"><?php echo $rs["City"]; ?></option>
		<?php
		}
		?>
		</select>
        
	
		<label style="font-size:20px" >Салон:</label>
		<select id="salon-list" name="salon"  >
		<option value="">Выберите Салон</option>
		</select>
		
		<label style="font-size:20px" >Мастер:</label>
		<select name="master" id="master-list">
		<option value="">Выберите Мастера</option>
		</select>
		
		<label style="font-size:20px" >
		Свободные дни<br>
		<table>
		<tr><td>Понедельник:</td><td><input type="checkbox" value="Monday" name="daylist[]"/></td></tr>
		<tr><td>Вторник:</td><td><input type="checkbox" value="Tuesday" name="daylist[]"/></td></tr>
		<tr><td>Среда:</td><td><input type="checkbox" value="Wednesday" name="daylist[]"/></td></tr>
		<tr><td>Четверг:</td><td><input type="checkbox" value="Thursday" name="daylist[]"/></td></tr>
		<tr><td>Пятница:</td><td><input type="checkbox" value="Friday" name="daylist[]"/></td></tr>
		<tr><td>Суббота:</td><td><input type="checkbox" value="Saturday" name="daylist[]"/></td></tr>
		</table>
		Availability(24 hour Format):<br>
		С:<input type="time" name="starttime"><br>
		До:<input type="time" name="endtime"> &nbsp &nbsp &nbsp
		
		</label>
		<button name="Submit" type="submit">Отправить</button>
	</form>
<?php
session_start();
if(isset($_POST['logout'])){
		session_unset();
		session_destroy();
		header( "Refresh:1; url=alogin.php"); 
	}
if(isset($_POST['Submit']))
{
		include 'dbconfig.php';
		$sid=$_POST['salon'];
		$mid=$_POST['master'];
		$starttime=$_POST['starttime'];
		$endtime=$_POST['endtime'];
		
		foreach($_POST['daylist'] as $daylist)
		{
				$sql = "INSERT INTO master_availability (SID, MID, Day, Starttime, Endtime) VALUES ('$sid','$mid','$daylist','$starttime','$endtime')";
				if (mysqli_query($conn, $sql)) 
				{
					echo "<h2>Record created successfully( SID=$sid MID=$mid Day=$daylist )!!</h2>";
				} 
				else
				{
					echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}
		}
}

?>
</div>
</body>
</html>