<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="adminmain.css"> 
<style>
table{
    width: 75%;
    border-collapse: collapse;
	border: 4px solid black;
    padding: 5px;
	font-size: 25px;
}

th{
border: 4px solid black;
	background-color: #4CAF50;
    color: white;
	text-align: left;
}
tr,td{
	border: 4px solid black;
	background-color: white;
    color: black;
}
</style>

</head>
<body>
<div id="main4">
<ul>
<li class="dropdown"><font color="yellow" size="10">Режим Админа</font></li>
<br>
<h2>
  <li class="dropdown">    
  <a href="javascript:void(0)" class="dropbtn">Мастер</a>
    <div class="dropdown-content">
      <a href="addmaster.php">Добавить Мастера</a>
      <a href="deletemaster.php">Удалить Мастера</a>
      <a href="showmaster.php">Список Мастеров</a>
	  <a href="showmasterschedule.php">Календарь Мастеров</a>
    </div>
  </li>
  
  <li class="dropdown">
  <a href="javascript:void(0)" class="dropbtn">Салон</a>
    <div class="dropdown-content">
      <a href="addsalon.php">Добавить Салон</a>
      <a href="deletesalon.php">Удалить Салон</a>
      <a href="addmastersalon.php">Добавить Мастера в Салон</a>
	   
	  <a href="deletemastersalon.php">Удалить Мастера с Салона</a>
	   
	  <a href="showsalon.php">Список Салонов</a>
    </div>
  </li>
 <li class="dropdown">    
		  <a href="javascript:void(0)" class="dropbtn">Менеджер</a>
			<div class="dropdown-content">
			  <a href="addmanagersalon.php">Добавить Менеджера в Салон</a>
			  <a href="deletemanagersalon.php">Удалить Менеджера из Салона</a>
			  <a href="addmanager.php">Добавить Менеджера</a>
			  <a href="deletemanager.php">Удалить Менеджера</a>
			  <a href="showmanager.php">Список Менеджеров</a>
    </div>
  </li>
   <li>  
	<form method="post" action="mainpage.php">	
	<button type="submit" class="cancelbtn" name="logout" style="float:right;font-size:22px"><b>Выйти</b></button>
	</form>
  </li>
	
</ul>
<center><h1>Календарь Мастеров</h1>
</h2>
<?php
session_start();
$con = mysqli_connect('localhost','root','','appointment');
if (!$con)
{
    die('Could not connect: ' . mysqli_error($con));
}
$sql="SELECT * FROM master_availability order by MID,SID ASC";
$result = mysqli_query($con,$sql);
echo "<br><h2>Общее количество салонов: <b>".mysqli_num_rows($result)."</b></h2><br>";
echo "<table>
<tr>
<th>SID</th>
<th>Salon Name</th>
<th>MID</th>
<th>Master Name</th>
<th>Day</th>
<th>Time</th>
</tr>";
while($row = mysqli_fetch_array($result))
{
	$sql1="SELECT * from master where MID=".$row["mid"];
	$result1= mysqli_query($con,$sql1);
	while($row1= mysqli_fetch_array($result1))
	{
	$sql2="SELECT * from salon where SID=".$row["sid"];
	$result2= mysqli_query($con,$sql2);
	while($row2= mysqli_fetch_array($result2))
	{
    echo "<tr>";
	echo "<td>" . $row['sid'] . "</td>";
    echo "<td>" . $row2['name']."-".$row2['town'] . "</td>";
	echo "<td>" . $row['mid'] . "</td>";
    echo "<td>" . $row1['name'] . "</td>";
	echo "<td>" . $row['day'] . "</td>";
    echo "<td>" . $row['starttime']."-".$row['endtime']. "</td>";
    echo "</tr>";
	}
	}
}
echo "</table>";
mysqli_close($con);
if(isset($_POST['logout'])){
		session_unset();
		session_destroy();
		header( "Refresh:1; url=alogin.php"); 
	}
?>
</div>
</body>
</html>