<html>
<head>
<link rel="stylesheet" href="main.css">
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
</head><?php include "dbconfig.php"; ?>
<script>
function getTown(val) {
	$.ajax({
	type: "POST",
	url: "get_town.php",
	data:'countryid='+val,
	success: function(data){
		$("#town-list").html(data);
	}
	});
}
function getSalon(val) {
	$.ajax({
	type: "POST",
	url: "getsalon.php",
	data:'townid='+val,
	success: function(data){
		$("#salon-list").html(data);
	}
	});
}
function getMasterday(val) {
	$.ajax({
	type: "POST",
	url: "getmasterdaybooking.php",
	data:'sid='+val,
	success: function(data){
		$("#master-list").html(data);
	}
	});
}

function getDay(val) {
	var sidval=document.getElementById("salon-list").value;
	var midval=document.getElementById("master-list").value;
	$.ajax({
	type: "POST",
	url: "getDay.php",
	data:'date='+val+'&sidval='+sidval+'&midval='+midval,
	success: function(data){
		$("#datestatus").html(data);
	}
	});
}

</script>
<body style="	background-color:white;">
	<div class="header">
		<ul>
			<li style="float:left;border-right:none"><a href="ulogin.php" class="logo"><img src="images/barb1.png" width="30px" height="30px"><strong> iSBooking </strong></a></li>
			<li><a href="book.php">Забронировать</a></li>
			<li><a href="ulogin.php">Главная</a></li>
		</ul>
	</div>
	<form action="book.php" method="post">
	<div class="sucontainer" style="background-image:url(images/bookback.jpg)">
		<label><b>Имя:</b></label><br>
		<input type="text" placeholder="Введите ваше полное имя" name="fname" required><br>
		
		<label><b>Пол:</b></label><br>
		<input type="radio" name="gender" value="female">Женщина
		<input type="radio" name="gender" value="male">Мужчина
		<input type="radio" name="gender" value="other">Другое<br><br>
	
		<label style="font-size:20px" >Город:</label><br>
		<select name="city" id="city-list" class="demoInputBox"  onChange="getTown(this.value);" style="width:100%;height:35px;border-radius:9px">
		<option value="">Выберите город</option>
		<?php
		$sql1="SELECT distinct(city) FROM salon";
         $results=$conn->query($sql1); 
		while($rs=$results->fetch_assoc()) { 
		?>
		<option value="<?php echo $rs["city"]; ?>"><?php echo $rs["city"]; ?></option>
		<?php
		}
		?>
		</select>
        <br>
	
		<label style="font-size:20px" >Район:</label><br>
		<select id="town-list" name="Town" onChange="getSalon(this.value);" style="width:100%;height:35px;border-radius:9px">
		<option value="">Выберите Район</option>
		</select><br>
		
		<label style="font-size:20px" >Салон:</label><br>
		<select id="salon-list" name="Salon" onChange="getMasterday(this.value);" style="width:100%;height:35px;border-radius:9px">
		<option value="">Выберите Салон</option>
		</select><br>
		
		<label style="font-size:20px" >Мастер:</label><br>
		<select id="master-list" name="Master" onChange="getDate(this.value);" style="width:100%;height:35px;border-radius:9px">
		<option value="">Выберите Мастера</option>
		</select><br>
		
		
		<label><b>Дата встречи:</b></label><br>
		<input type="date" name="dov" onChange="getDay(this.value);" min="<?php echo date('Y-m-d');?>" max="<?php echo date('Y-m-d',strtotime('+7 day'));?>" required><br><br>
		<div id="datestatus"> </div>
		
		<div class="container">
			<button type="submit" style="position:center" name="submit" value="Submit">Отправить</button>
		</div>
<?php
session_start();
if(isset($_POST['submit']))
{
		
		include 'dbconfig.php';
		$fname=$_POST['fname'];
		$gender=$_POST['gender'];
		$username=$_SESSION['username'];
		$sid=$_POST['Salon'];
		$mid=$_POST['Master'];
		$dov=$_POST['dov'];
		$status="Booking Registered.Wait for the update";
		$timestamp=date('Y-m-d H:i:s');
		$sql = "INSERT INTO book (Username,Fname,Gender,SID,MID,DOV,Timestamp,Status) VALUES ('$username','$fname','$gender','$sid','$mid','$dov','$timestamp','$status') ";
		if(!empty($_POST['fname'])&&!empty($_POST['gender'])&&!empty($_SESSION['username'])&&!empty($_POST['Salon'])&&!empty($_POST['Master']) && !empty($_POST['dov']))
		{
			$checkday = strtotime($dov);
			$compareday = date("l", $checkday);
			$flag=0;
			require_once("dbconfig.php");
			$query ="SELECT * FROM master_availability WHERE MID = '" .$mid. "' AND SID='".$sid."'";
			$results = $conn->query($query);
			while($rs=$results->fetch_assoc())
			{
				if($rs["day"]==$compareday)
				{
					$flag++;
					break;
				}
			}
			if($flag==0)
			{
				echo "<h2>Select another date as Master Unavailable on ".$compareday."</h2>";
			}
			else
			{
				if (mysqli_query($conn, $sql)) 
				{
						echo "<h2>Booking successful!! Redirecting to home page....</h2>";
						header( "Refresh:2; url=ulogin.php");

				} 
				else
				{
					echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}
			}
		}
		else
		{
			echo "Enter data properly!!!!";
		}
}
?>
	</form>
</body>
</html>